package team.pairfy.herokusample.infra.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import team.pairfy.herokusample.model.MobilePhoneNumberDto;

@RestController
public class HerokuRestController {



    @GetMapping("heroku")
    public String heroku(){
        return "heroku";
    }

    @GetMapping("17329074148")
    public MobilePhoneNumberDto tc(){
        MobilePhoneNumberDto mobilePhoneNumberDto = new MobilePhoneNumberDto();
        mobilePhoneNumberDto.setNumber("05350194342");
        return mobilePhoneNumberDto;
    }


    @GetMapping("17338073866")
    public MobilePhoneNumberDto tc_(){
        MobilePhoneNumberDto mobilePhoneNumberDto = new MobilePhoneNumberDto();
        mobilePhoneNumberDto.setNumber("05342300204");
        return mobilePhoneNumberDto;
    }

}
