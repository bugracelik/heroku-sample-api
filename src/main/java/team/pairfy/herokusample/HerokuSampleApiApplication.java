package team.pairfy.herokusample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HerokuSampleApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HerokuSampleApiApplication.class, args);
	}

}
