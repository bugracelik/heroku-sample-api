package team.pairfy.herokusample.model;

public class MobilePhoneNumberDto {
    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
